[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/redrudeboy/gitpod-phalcon-example) 

# Gitpod Phalcon Example

Start server:
```bash
php -S localhost:8000 -t public/
$(which php) -S localhost:8000 -t public .htrouter.php
service nginx start
service apache start
```

Phalcon devtools cli
```bash
./vendor/phalcon/devtools/phalcon
```

To Do:
* [Add postgresql](https://www.gitpod.io/blog/gitpodify/), check:  `psql -h localhost -d postgres`

Info:
* [More info about env vars](https://www.gitpod.io/docs/environment-variables/)
