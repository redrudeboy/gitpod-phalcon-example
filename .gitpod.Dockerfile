#FROM gitpod/workspace-full
FROM leninux/gitpod-phalcon
                    
#USER gitpod

# Install custom tools, runtime, etc. using apt-get
# For example, the command below would install "bastet" - a command line tetris clone:
#
# RUN sudo apt-get -q update && #     sudo apt-get install -yq bastet && #     sudo rm -rf /var/lib/apt/lists/*
#
# More information: https://www.gitpod.io/docs/42_config_docker/
USER root
ENV DEBIAN_FRONTEND noninteractive

#RUN curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | bash
#RUN apt-cache search phalcon
#RUN apt-get update && apt-get install -y && apt-get dist-upgrade && apt-get autoremove && apt-get clean

#lsb_release -cs disco
#lsb_release -is Ubuntu

RUN add-apt-repository ppa:ondrej/php && \
    add-apt-repository ppa:ondrej/apache2 && \
    curl -L https://packagecloud.io/phalcon/stable/gpgkey | sudo apt-key add - && \
    sh -c 'echo "deb https://packagecloud.io/phalcon/stable/ubuntu/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/phalcon_stable.list' && \
    sh -c 'echo "deb-src https://packagecloud.io/phalcon/stable/ubuntu/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/phalcon_stable.list' && \
#    echo "deb https://packagecloud.io/phalcon/stable/ubuntu/ disco main" > /etc/apt/sources.list.d/phalcon_stable.list && \
#    echo "deb-src https://packagecloud.io/phalcon/stable/ubuntu/ disco main" >> /etc/apt/sources.list.d/phalcon_stable.list && \
    apt-get update && apt-get install -y apt-utils gcc libpcre3-dev software-properties-common curl gnupg apt-transport-https && \
#   apt-get dist-upgrade -y && apt-get autoremove -y && apt-get clean && \
    apt-get update && \
    apt-get install -y php php-curl php-gd php-json php-mbstring && \
    apt-cache search phalcon* && \
#    apt-cache search php-ph* && \
    apt-get dist-upgrade -y && \
    apt-get install -y php-phalcon4 && \
    apt-get autoremove -y && apt-get autoclean
#   && rm -rf /var/lib/apt/lists/*
